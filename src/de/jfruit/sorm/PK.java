package de.jfruit.sorm;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Defines this field as a part of the primary key. 
 * SORM can handle primary keys containing multiple fields.
 *  
 * @author Tomate_Salat
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface PK {

}
