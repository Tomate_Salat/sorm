package de.jfruit.sorm.model.db;

import de.jfruit.sorm.Column;
import de.jfruit.sorm.SORM;

@SORM("sqlite_master")
public final class SqliteMaster
{
	private String sql;
	private String type;
	private int rootPage;
	private String name;
	
	@Column("tbl_name")
	private String tblName;

	public String getSql()
	{
		return sql;
	}

	public void setSql(final String sql)
	{
		this.sql = sql;
	}

	public int getRootPage()
	{
		return rootPage;
	}

	public void setRootPage(final int rootPage)
	{
		this.rootPage = rootPage;
	}

	public String getName()
	{
		return name;
	}

	public void setName(final String name)
	{
		this.name = name;
	}

	public String getTblName()
	{
		return tblName;
	}

	public void setTblName(final String tblName)
	{
		this.tblName = tblName;
	}

	public String getType()
	{
		return type;
	}

	public void setType(final String type)
	{
		this.type = type;
	}	
}
