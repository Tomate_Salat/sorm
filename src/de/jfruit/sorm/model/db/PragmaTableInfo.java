package de.jfruit.sorm.model.db;

import de.jfruit.sorm.SORM;

@SORM
public final class PragmaTableInfo
{
	private int cid;
	private String name;
	private String type;
	private int notnull;
	private int pk;

	public int getCid()
	{
		return cid;
	}
	public void setCid(final int cid)
	{
		this.cid = cid;
	}
	public String getName()
	{
		return name;
	}
	public void setName(final String name)
	{
		this.name = name;
	}
	public String getType()
	{
		return type;
	}
	public void setType(final String type)
	{
		this.type = type;
	}
	public int getNotnull()
	{
		return notnull;
	}
	public void setNotnull(final int notnull)
	{
		this.notnull = notnull;
	}
	public int getPk()
	{
		return pk;
	}
	public void setPk(final int pk)
	{
		this.pk = pk;
	}
}
