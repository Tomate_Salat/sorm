package de.jfruit.sorm.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class STable 
{
	private String name;
	private List<SColumn> columns=new ArrayList<SColumn>();
	
	private List<String> oneToOneFields=new ArrayList<String>();
	private List<String> primaryKey=new ArrayList<String>();
	private List<SIndex> indexes=new ArrayList<SIndex>();	

	private String orderBy="";
	
	public STable(final String name) 
	{
		this.name = name;
	}

	public String getName() 
	{
		return name;
	}

	public void setName(final String name) 
	{
		this.name = name;
	}

	public String getOrderBy()
	{
		return orderBy;
	}
	
	public void setOrderBy(final String orderBy)
	{
		if(orderBy==null) {
			this.orderBy="";
			return;
		}
		
		this.orderBy = orderBy;
	}
	
	public void addColumn(final SColumn column)
	{
		columns.add(column);
	}
	
	public List<SColumn> getColumns() 
	{
		return new ArrayList<SColumn>(columns);
	}

	public void clearColumns()
	{
		columns.clear();
	}
	
	public List<String> getPrimaryKey() 
	{
		return primaryKey;
	}

	public void addPrimaryKeyField(final String field)
	{
		primaryKey.add(field);
	}
	
	public void addPrimaryKeyField(final SColumn field)
	{
		primaryKey.add(field.getName());
	}
	
	public void addIndex(final SIndex index)
	{
		indexes.add(index);
	}
	
	public void addOneToOneField(final String fieldName)
	{
		oneToOneFields.add(fieldName);
	}
	
	public List<String> getOneToOneFields()
	{
		return new ArrayList<String>(oneToOneFields);
	}
	
	public List<SIndex> getIndexes() 
	{
		return indexes;
	}
	
	public void sortIndexes()
	{
		Collections.sort(indexes);
	}
	
	public boolean hasOneToOne()
	{
		return !oneToOneFields.isEmpty();
	}
}
