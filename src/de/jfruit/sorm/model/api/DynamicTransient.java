package de.jfruit.sorm.model.api;

import java.lang.reflect.Field;

import de.jfruit.sorm.SORMFactory;

/**
 * Interface for declaring Fields dynamically as Transient 
 */
public interface DynamicTransient
{
	/**
	 * Check the given Field on Runtime and set it as transient if needed.
	 * The method should only return <code>true</code> if the given field should be
	 * transient.
	 * 
	 * @return true if the field is transient and should be ignored by {@link SORMFactory}.
	 */
	boolean isFieldTransient(Field field);
}
