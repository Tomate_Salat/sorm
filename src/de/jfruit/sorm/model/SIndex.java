package de.jfruit.sorm.model;

import de.jfruit.sorm.Index;

public class SIndex implements Comparable<SIndex>
{
	private String name, field;
	private int order;
	
	public SIndex() {
		this("","",1);
	}

	public SIndex(SColumn column, Index index)
	{
		this(index.value(), column.getName(), index.order());
	}
	
	public SIndex(String name, String field, int order) 
	{
		setName(name);
		setField(field);
		setOrder(order);
	}
	
	public String getName() 
	{		
		return name;
	}
	
	public void setName(String name) 
	{
		this.name = (name==null) ? "" : name.toLowerCase();
	}
	
	public int getOrder() 
	{
		return order;
	}
	
	public void setOrder(int order) 
	{
		this.order = order;
	}

	public String getField() 
	{
		return field;
	}
	
	public void setField(String field) 
	{
		this.field = field;
	}
	
	@Override
	public int compareTo(SIndex o)
	{
		if(o==null)
			return -1;
		
		if(!o.name.equals(name))
			return o.name.compareTo(name);				
		
		return Integer.compare(order, o.order);
	}
}
