package de.jfruit.sorm.model.prov;

import java.util.ArrayList;
import java.util.List;

import de.jfruit.sorm.model.ToManyProvider;

public class ListToManyProvider implements ToManyProvider
{
	List<Object> o=new ArrayList<Object>();
	
	@Override
	public void add(final Object object)
	{
		o.add(object);
	}

	@Override
	public Object getHolderObject()
	{
		return o;
	}
}
