package de.jfruit.sorm.model.prov;

import java.util.HashSet;
import java.util.Set;

import de.jfruit.sorm.model.ToManyProvider;

public class SetToManyProvider implements ToManyProvider
{
	private Set<Object> set=new HashSet<Object>();
	
	@Override
	public void add(final Object object)
	{
		set.add(object);
	}

	@Override
	public Object getHolderObject()
	{
		return set;
	}
	
}
