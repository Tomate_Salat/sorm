package de.jfruit.sorm.model;

public interface ToManyProvider
{
	void add(Object object);
	
	Object getHolderObject();
}
