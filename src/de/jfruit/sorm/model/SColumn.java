package de.jfruit.sorm.model;


public class SColumn 
{
	private String name;
	private String type;
	private boolean autoInc;	
	private String javaFieldName;
	
	public SColumn()
	{
		this(null,null);
	}
	
	public SColumn(final String name) 
	{
		this(name,null);
	}
	
	public SColumn(final String name, final String type) 
	{
		this.name = name;
		this.type = type;
		this.autoInc = false;
	}

	public String getName() 
	{
		return name;
	}

	public void setName(final String name) 
	{
		this.name = name;
	}

	public String getType() 
	{
		return type;
	}

	public void setType(final String type) 
	{
		this.type = type;
	}
	
	public String getJavaFieldName()
	{
		return javaFieldName;
	}
	
	public void setJavaFieldName(final String javaFieldName)
	{
		this.javaFieldName = javaFieldName;
	}
	
	public void setAutoInc(final boolean autoInc) 
	{
		this.autoInc = autoInc;
	}
	
	public boolean isAutoInc() 
	{
		return autoInc;
	}
}
