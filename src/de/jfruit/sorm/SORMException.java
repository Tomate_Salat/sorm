package de.jfruit.sorm;

/**
 * {@link SORMFactory} use this Exception if problems will have been occured.
 * This class provides several Messages as Exception used by the caller.
 * 
 * @author Tomate_Salat
 */
public class SORMException extends RuntimeException 
{
	private static final long serialVersionUID = 1L;

	public static enum Type {
		MISSING_SORM, NO_DATA;		
	}
	
	final static String[] MESSAGES={
		"Annotation de.jfruit.sorm.SORM must be present!",
		"No data received"
	};	
	
	public SORMException(Type type) {
		this(type.ordinal());
	}
	
	SORMException(int index) {
		super(MESSAGES.length<=index ? "unknown Exception" : MESSAGES[index]);
	}
}
