package de.jfruit.sorm;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteDatabase;
import de.jfruit.sorm.SORMException.Type;
import de.jfruit.sorm.model.SColumn;
import de.jfruit.sorm.model.SIndex;
import de.jfruit.sorm.model.STable;
import de.jfruit.sorm.model.ToManyProvider;
import de.jfruit.sorm.model.api.DynamicTransient;
import de.jfruit.sorm.model.db.PragmaTableInfo;
import de.jfruit.sorm.model.prov.ListToManyProvider;
import de.jfruit.sorm.model.prov.SetToManyProvider;

/**
 * {@link SORMFactory} provides a simple ORM. To use a class with SORMFactory it
 * has to be annotated with {@link SORM}.
 * 
 * @author Tomate_Salat
 */
public class SORMFactory 
{
	private SORMFactory() {}
	private final static Map<Class<?>, STable> tableModel=new HashMap<Class<?>, STable>();
	
	private static Map<Class<? extends Annotation>, DynamicTransient> dynamicTransientMap=new HashMap<Class<? extends Annotation>, DynamicTransient>();
	
	private static class QuerySceleton
	{
		String qSelect="";
		String qFrom="";
		String qJoin="";
		String qWhere="";
		String qOrderBy="";
		String qHaving="";
		
		@Override
		public String toString()
		{
			return qSelect + qFrom + qJoin + qWhere + qHaving + qOrderBy;
		}
	}
	
	public static boolean registerDynamicTransient(final Class<? extends Annotation> annotation, final DynamicTransient dynamicTransient)
	{
		if(dynamicTransient!=null && annotation!=null) {
			dynamicTransientMap.put(annotation, dynamicTransient);
			return true;
		}
		
		return false;
	}
	
	public boolean unregisterDynamicTransient(final Class<? extends Annotation> annotation)
	{
		if(annotation==null || !dynamicTransientMap.containsKey(annotation))
			return false;
		
		dynamicTransientMap.remove(annotation);
		
		return true;
	}
	
	/**
	 * This method creates the TableModel to the given class. 
	 * Other methods may will call this method to do their work. E.g. {@link SORMFactory#createTable(Class)}
	 * requires this method to generate the SQL-Statement. This method will cache each TableModel.  
	 * 
	 * @param clazz that is annotated with {@link SORM}
	 * @return {@link STable}
	 */
	public static STable createTableModel(final Class<?> clazz)
	{
		// Check Annotation
		if(!clazz.isAnnotationPresent(SORM.class))
			throw new SORMException(Type.MISSING_SORM);
		
		// Check Cache
		if(tableModel.containsKey(clazz))
			return tableModel.get(clazz);
		
		// Init STable
		STable table=new STable(getTableName(clazz));		
		
		SORM sorm=clazz.getAnnotation(SORM.class);
		boolean columnAnnoRequired=sorm.columnAnnoRequired();
		
		Class<?> cla=clazz;
		String orderBy="";
		do {
			// Check Fields
			for(Field field:cla.getDeclaredFields()) {
				if(field.isSynthetic() || Modifier.isTransient(field.getModifiers()) || field.isAnnotationPresent(OneToMany.class))
					continue;								
			
				boolean isDynamicTransient=false;
				if(!dynamicTransientMap.isEmpty()) {
					for(Map.Entry<Class<? extends Annotation>, DynamicTransient> entry:dynamicTransientMap.entrySet()) {						
						
						if(field.isAnnotationPresent(entry.getKey()))
							isDynamicTransient=entry.getValue().isFieldTransient(field);
					}
				}
				
				if(isDynamicTransient) 
					continue;
				
				boolean columnAnnotationPresent=field.isAnnotationPresent(Column.class);
				if(!columnAnnotationPresent && columnAnnoRequired)
					continue;
		
				if(field.isAnnotationPresent(OrderBy.class)) {
					OrderBy order=field.getAnnotation(OrderBy.class);
					Order o=order.value();

					orderBy+=(orderBy.length()==0) 
						? getColName(field) + " " + o.name()
						: ", " + getColName(field) + " " + o.name() 
					;
				}
				
				if(field.isAnnotationPresent(OneToOne.class)) {
					table.addOneToOneField(field.getName());
					continue;
				}
				
				// ORM-Field found
				SColumn column=new SColumn(getColName(field));
				column.setJavaFieldName(field.getName());
	
				// Handle ColumnAnnotation
				String fieldType=(columnAnnotationPresent) ? field.getAnnotation(Column.class).type().trim() : "";
				
				Class<?> type=field.getType();
				
				if(fieldType.length()==0) {
					Map<String, Class<?>[]> typeMap=new HashMap<String, Class<?>[]>();			
					typeMap.put("INTEGER", new Class[] {Integer.class,Integer.TYPE, Long.class, Long.TYPE});
					typeMap.put("TEXT", new Class[] {String.class,Character.TYPE, Character.class});
					typeMap.put("REAL", new Class[] {Float.class, Float.TYPE, Double.class, Double.TYPE});
					typeMap.put("BOOLEAN", new Class[] {Boolean.class, Boolean.TYPE});
					typeMap.put("BLOB", new Class[] {Byte[].class,byte[].class});
					
					boolean found=false;
					for(Map.Entry<String, Class<?>[]> entry:typeMap.entrySet()) {
						if(found)
							break;
						
						for(Class<?> cl : entry.getValue()) {
							if(type.equals(cl)) {
								fieldType=entry.getKey();
								found=true;
								break;
							}
						}
					}
				} 
				
				column.setType(fieldType);
				
				// register AutoInc
				if(field.isAnnotationPresent(Autoinc.class))
					column.setAutoInc(true);
				
				// register Primary Keys
				if(field.isAnnotationPresent(PK.class)) 
					table.addPrimaryKeyField(column);
				
				// register Index(es)
				if(field.isAnnotationPresent(Index.class))
					table.addIndex(new SIndex(column, field.getAnnotation(Index.class)));
				
				if(field.isAnnotationPresent(Indexes.class)) {
					for(Index index:field.getAnnotation(Indexes.class).value()) {
						table.addIndex(new SIndex(column, index));
					}
				}			
	
				table.addColumn(column);
			}
			
			cla=cla.getSuperclass();
		} while(cla!=Object.class && cla!=null && sorm.checkParentClass());

		table.setOrderBy(orderBy);
		tableModel.put(clazz, table);
		
		return table;
	}
	
	/**
	 * See {@link SORMFactory#createTable(Class)}.<br>
	 * <br>
	 * Short for:
	 * database.execSQL(createTable(clazz));
	 * 
	 * @param database
	 * @param clazz
	 */
	public static void createTable(final SQLiteDatabase database, final Class<?> clazz)
	{
		database.execSQL(createTable(clazz));
	}
	
	/**
	 * Creates or Updates a table. 
	 * The method only updates missing columns. Be carefull - no unittest is written for this Method at the moment. 
	 * This will come in future, when I have converted the project to an Android-Project
	 * 
	 * @param database
	 * @param clazz
	 */
	public static void createOrUpdateTable(final SQLiteDatabase database, final Class<?> clazz)
	{
		// TODO write an JUnit-Test and after that, edit this methods javadoc!
		if(!checkTableExists(database, clazz)) {
			createTable(database, clazz);
			return;
		}
		
		STable tableModel=createTableModel(clazz);
		Map<String, SColumn> columnMap=new HashMap<String, SColumn>();
		
		for(SColumn column:tableModel.getColumns())
			columnMap.put(column.getName().toUpperCase(), column);
		
		Cursor cursor=database.rawQuery("PRAGMA table_info(" + tableModel.getName() + ");", null);
		
		if(!cursor.moveToFirst()) return;
		
		do {
			PragmaTableInfo tableInfo=select(PragmaTableInfo.class, cursor);
			columnMap.remove(tableInfo.getName().toUpperCase());			// only keep non-existing columns
		} while(cursor.moveToNext());
		
		String baseSQL="ALTER TABLE " + tableModel.getName() + " ADD COLUMN ";
		
		// Add Columns
		for(Entry<String, SColumn> entry:columnMap.entrySet()) {
			SColumn col=entry.getValue();
			database.execSQL(baseSQL + col.getName() + " " + col.getType() + ";");
		}
	}

	/**
	 * Create a statement to check if a table is existing or not.
	 */
	public static String createCheckTableExists(final Class<?> clazz)
	{
		STable table=createTableModel(clazz);
		
		return "SELECT COUNT(*) FROM sqlite_master WHERE type='table' AND name='" + table.getName() + "';";
	}
	
	/**
	 *  Uses the statement of {@link SORMFactory#createCheckTableExists(Class)} and executes it on the
	 *  given database. Also the method checks if the result is greater then 0. If this is true -> the table exists.
	 *  
	 *  @throws
	 *  SORMException if the statement wasn't able to be executed
	 */
	public static boolean checkTableExists(final SQLiteDatabase database, final Class<?> clazz)
	{
		Cursor cursor=database.rawQuery(createCheckTableExists(clazz), null);
		
		if(!cursor.moveToFirst())
			throw new SORMException(Type.NO_DATA);
		
		return cursor.getInt(0)>0;
	}
	
	/**
	 * Generates the create-Statement for the given class and returns it. 
	 * But keep in mind the class have to be annotated with {@link SORM}
	 * 
	 * @param clazz must be annotated with {@link SORM}
	 * @return create-Statement
	 */
	public static String createTable(final Class<?> clazz)
	{
		if(!clazz.isAnnotationPresent(SORM.class))
			throw new SORMException(Type.MISSING_SORM);
		
		STable tableData=createTableModel(clazz);
		
		StringBuilder createStatement=new StringBuilder();
		createStatement.append("CREATE TABLE IF NOT EXISTS " + tableData.getName() + " (");

		boolean addComma=false;
		
		for(SColumn column:tableData.getColumns()) {
			if(addComma)
				createStatement.append(",");
						
			createStatement.append(column.getName() + " " + column.getType());
			
			if(column.isAutoInc())
				createStatement.append(" auto increment");
			
			addComma=true;
		}
		
		List<String> pkFields=tableData.getPrimaryKey();
		
		if(!pkFields.isEmpty()) {
			createStatement.append(",PRIMARY KEY(");
			int pkCount=pkFields.size()-1;
			for(int i=0;i<=pkCount;i++) {
				createStatement.append(pkFields.get(i));
				if(i<pkCount)
					createStatement.append(",");
			}
			createStatement.append(")");
		}
		
		createStatement.append(");");
		
		createIndexStatement(tableData, createStatement);
		
		return createStatement.toString();
	}
	
	private static void createIndexStatement(final STable tableData, final StringBuilder createStatement)
	{
		tableData.sortIndexes();
		
		String currentIdx=null;
		List<SIndex> indexes=tableData.getIndexes();
		for(SIndex idx:indexes) {
			if(!idx.getName().equals(currentIdx)) {
				if(currentIdx!=null)
					createStatement.append(");");
				
				currentIdx=idx.getName();
				createStatement.append("CREATE INDEX IF NOT EXISTS " + currentIdx + " ON " + tableData.getName() + "(" + idx.getField());
				continue;
			}
			
			createStatement.append("," + idx.getField());
		}
		
		if(currentIdx!=null)
			createStatement.append(");");
	}
	
	public static String createIndexStatement(final Class<?> table)
	{
		if(!table.isAnnotationPresent(SORM.class))
			throw new SORMException(SORMException.Type.MISSING_SORM);
		
		STable tableData=createTableModel(table);
		
		StringBuilder createStmt=new StringBuilder();
		
		createIndexStatement(tableData, createStmt);
		
		return createStmt.toString();
	}
	
	/**
	 * Creates a statement like SELECT * FROM [table]. 
	 * If there are {@link OneToOne}+{@link JoinColumn}-Annotated fields, the method will also use
	 * left joins to get this values. 
	 * 
	 * @param clazz the class (annotated with {@link SORM}) that should be selected
	 * @return SELECT-SQL-Statement
	 */
	public static String createSelectAll(final Class<?> clazz)
	{
		return createSelectAll(clazz, null, null, null);
	}	
	
	/**
	 * @see #createSelectAll(Class)
	 */
	public static String createSelectAll(final Class<?> clazz, final String where)
	{
		return createSelectAll(clazz, where, null, null);
	}

	/**
	 * @see #createSelectAll(Class)
	 */
	public static String createSelectAll(final Class<?> clazz, final String where, final String orderBy)
	{
		return createSelectAll(clazz, where, orderBy, null);
	}
	
	/**
	 * @see #createSelectAll(Class)
	 */
	public static String createSelectAll(final Class<?> clazz, final String where, final String orderBy, final String having)
	{
		if(!clazz.isAnnotationPresent(SORM.class))
			throw new SORMException(Type.MISSING_SORM);
		
		STable table=createTableModel(clazz);
		QuerySceleton sceleton = new QuerySceleton();
				
		sceleton.qSelect="SELECT " + table.getName() + ".*";
		sceleton.qFrom=" FROM " + table.getName();		
		
		if(where!=null && where.trim().length()>0)						
			sceleton.qWhere+=" WHERE " + where.trim();
				
		if(having!=null && having.trim().length()>0)
			sceleton.qHaving+=" HAVING " + having.trim();
		
		if(orderBy!=null && orderBy.trim().length()>0)
			sceleton.qOrderBy+=" ORDER BY " + orderBy.trim();		
		
		if(table.getOrderBy().length()>0) {
			sceleton.qOrderBy+=(sceleton.qOrderBy.length()>0) 
				? ", " + table.getOrderBy()
				: " ORDER BY " + table.getOrderBy()
			;
		}
		
		// Check oneToOne
		for(String fld:table.getOneToOneFields())
			createLeftJoinStatement(clazz, fld, table, sceleton,"");
		
		String query=sceleton.toString();
		
		query=query.replace("${TABLE}", table.getName());
		
		return query+";";
	}

	private static void createLeftJoinStatement(final Class<?> clazz, final String fld, final STable table, final QuerySceleton sceleton, final String tableAlias)
	{
		try {
			Field field=clazz.getDeclaredField(fld);
			field.setAccessible(true);
			
			boolean hasTableAlias=tableAlias!=null && !tableAlias.trim().isEmpty();
			
			JoinColumn[] joinCols=null;
			
			if(!field.isAnnotationPresent(JoinColumns.class)) {
				if(!field.isAnnotationPresent(JoinColumn.class))
					return;
				
				joinCols=new JoinColumn[] {field.getAnnotation(JoinColumn.class)};
			} else
				joinCols=field.getAnnotation(JoinColumns.class).value();
			
			if(joinCols.length==0)
				return;

			Class<?> fieldType=field.getType();
			STable joinTableModel=createTableModel(fieldType);
							
			String jTable=joinTableModel.getName();
			String jAlias=(hasTableAlias ? tableAlias + "_" : "") + field.getName();
			
			sceleton.qSelect+="," + getColumnsAsSelect(joinTableModel, jAlias);
			sceleton.qJoin+=" LEFT JOIN " + jTable + " " + jAlias + " ON ";
			
			for(int i=0;i<joinCols.length;i++) {
				JoinColumn joinColumn=joinCols[i];				
				if(i>0) 
					sceleton.qJoin+=" AND ";
				
				// bind to an table field
				sceleton.qJoin+=jAlias + ".";					
				String refColumn=joinColumn.referencedColumnName();

				if(refColumn!=null && refColumn.trim().length()>0)
					sceleton.qJoin+=refColumn.trim();
				else
					sceleton.qJoin+=joinColumn.name();
				
				sceleton.qJoin+="=";
				
				String bindTableName=hasTableAlias ? tableAlias : table.getName();
				
				// Check if the user want to use a fixed value
				String fixVal=joinColumn.toValue();					
				if(fixVal!=null && fixVal.trim().length()>0) {
					sceleton.qJoin+="'"+fixVal.trim()+"'";
					continue;
				} else
					sceleton.qJoin+=bindTableName + "." + joinColumn.name();
			}
			
			if(joinTableModel.hasOneToOne()) {
				for(String jFld:joinTableModel.getOneToOneFields())
					createLeftJoinStatement(fieldType, jFld, joinTableModel, sceleton, jAlias);
			}
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * This method tries to store the data (cursor) in a new Object of the
	 * given class. {@link SORMFactory} will try to store the data via Reflection.
	 * 
	 * @param clazz annotated with {@link SORM}.
	 * @param cursor the data should be stored
	 * @return a instance of clazz with the given data
	 */
	public static <T> T select(final Class<T> clazz, final Cursor cursor)
	{
		if(!clazz.isAnnotationPresent(SORM.class)) {
			throw new SORMException(Type.MISSING_SORM);
		}
		
		T instance = null;
		try {
			instance = clazz.newInstance();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		
		return select(instance, cursor, null);
	}
	
	public static <T> T select(final T object, final Cursor cursor) {
		return select(object, cursor, null);
	}
	
	private static <T> T select(final T instance, final Cursor cursor, final String alias)
	{					
		Class<?> clazz = instance.getClass();
		
		SORM sorm=clazz.getAnnotation(SORM.class);
		boolean isColumnAnnotationRequired=sorm.columnAnnoRequired();		
		
		Class<?> cl=clazz;
		String colPrefix=(alias==null) ? "" /*getTableName(clazz) + "_"*/ : alias + "_";
		
		do {
			for(Field field:cl.getDeclaredFields()) {
				if(field.isSynthetic() || Modifier.isTransient(field.getModifiers()))
					continue;
				
				if(!field.isAnnotationPresent(Column.class) && isColumnAnnotationRequired)
					continue;
				
				field.setAccessible(true);
				
				if(field.isAnnotationPresent(OneToMany.class))
					initOneToMany(cursor, instance, field);
				
				if(field.isAnnotationPresent(OneToOne.class)) {
					String newAlias=((alias!=null && !alias.isEmpty()) ? alias + "_" : "") + field.getName();							
					Object otoValue=SORMFactory.select(field.getType(),cursor,newAlias);
					
					try {
						field.set(instance, otoValue);
					} catch(Exception e) {
						e.printStackTrace();
					}
					
					continue;
				}
				String colName=colPrefix+getColName(field);
				int index=cursor.getColumnIndex(colName);
				
				// Index konnte nicht ermittelt werden
				if(index==-1)
					continue;
				
				Class<?> type=field.getType();
				
				try {
					if(type.equals(String.class) || type.equals(Character.class) || type.equals(Character.TYPE)) 
						field.set(instance, cursor.getString(index));
					else if(type.equals(Integer.class) || type.equals(Integer.TYPE))
						field.set(instance, cursor.getInt(index));
					else if(type.equals(Long.class) || type.equals(Long.TYPE))
						field.set(instance, cursor.getLong(index));					
					else if(type.equals(Short.class) || type.equals(Short.TYPE))
						field.set(instance, cursor.getShort(index));
					else if(type.equals(Double.class) || type.equals(Double.TYPE)) 
						field.set(instance, cursor.getDouble(index));
					else if(type.equals(Float.class) || type.equals(Float.TYPE)) 
						field.set(instance, cursor.getFloat(index));
					else if(type.equals(Byte[].class) || type.equals(byte[].class))
						field.set(instance, cursor.getBlob(index));
					else if(type.equals(Boolean.class) || type.equals(Boolean.TYPE))
						field.set(instance, cursor.getInt(index)>0);
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
			
			cl=cl.getSuperclass();
		} while(cl!=Object.class && cl!=null && sorm.checkParentClass());
		
		return instance;
	}
	
	private static void initOneToMany(final Cursor cursor, final Object instance, final Field field)
	{		
		Class<?> fieldType=field.getType();
		
		ToManyProvider provider=null;
		
		if(fieldType.isAssignableFrom(List.class))
			provider=new ListToManyProvider();
		else if(fieldType.isAssignableFrom(Set.class))
			provider=new SetToManyProvider();
		
		// TODO Exception
		if(provider==null) 
			return;
		
		JoinColumn[] joinCols=null;
		
		if(!field.isAnnotationPresent(JoinColumns.class)) {
			if(!field.isAnnotationPresent(JoinColumn.class)) return;
			
			joinCols=new JoinColumn[] {field.getAnnotation(JoinColumn.class)};
		} else
			joinCols=field.getAnnotation(JoinColumns.class).value();		

		String where="";
		
		for(int i=0;i<joinCols.length;i++) {
			JoinColumn joinColumn=joinCols[i];				
			if(i>0) 
				where+=" AND ";
						
			String refColumn=joinColumn.referencedColumnName();
			
			where+="${TABLE}.";
			if(refColumn!=null && refColumn.trim().length()>0)
				where+=refColumn.trim();
			else
				where+=joinColumn.name();
			
			where+="=";
			
			// Check if the user want to use a fixed value
			String fixVal=joinColumn.toValue();					
			if(fixVal!=null && fixVal.trim().length()>0)
				where+="'"+fixVal.trim()+"'";
			else
				where+="'"+ cursor.getString(cursor.getColumnIndex(joinColumn.name())) +"'";
		}
		
		Class<?> tableClass=field.getAnnotation(OneToMany.class).table();
		
		SQLiteDatabase sqlite=((SQLiteCursor)cursor).getDatabase();
		Cursor rawCursor=sqlite.rawQuery(createSelectAll(tableClass, where), null);		
		
		try {
			if(rawCursor.moveToFirst()) {
				do {
					provider.add(select(tableClass, rawCursor));
				} while(rawCursor.moveToNext());
			}
			
			try {
				field.set(instance, provider.getHolderObject());
			} catch(Exception e) {
				e.printStackTrace();
			}
		} finally {
			rawCursor.close();
		}
	}
	
	/**
	 * Create and fills {@link ContentValues} with the given data of the given Object.
	 * Only fields annotated with {@link Column} will be used to fill ContentValues. 
	 * Also of course objects class has to be annotated with {@link SORM}!!!
	 * 
	 * @param object hmm...the object
	 * @return ContentValues filled with the data of object
	 */
	public static ContentValues toContentValue(final Object object)
	{
		Class<?> clazz=object.getClass();
		
		if(!clazz.isAnnotationPresent(SORM.class))
			throw new SORMException(Type.MISSING_SORM);
		
		SORM sorm=clazz.getAnnotation(SORM.class);
		
		ContentValues values=new ContentValues();
		
		do {
			for(Field field:clazz.getDeclaredFields()) {
				field.setAccessible(true);
				
				if(Modifier.isTransient(field.getModifiers())) continue;
				
				if(field.isAnnotationPresent(Autoinc.class)) continue;
				
				if(!field.isAnnotationPresent(Column.class) && sorm.columnAnnoRequired())
					continue;
				
				String colName=getColName(field);
				
				Class<?> type=field.getType();
				
				try {
					if(type.equals(String.class) || type.equals(Character.class) || type.equals(Character.TYPE)) { 
						Object objVal=field.get(object);
						values.put(colName, objVal==null ? null : objVal.toString());
					} else if(type.equals(Integer.class) || type.equals(Integer.TYPE))
						values.put(colName, field.getInt(object));
					else if(type.equals(Long.class) || type.equals(Long.TYPE))
						values.put(colName, field.getLong(object));					
					else if(type.equals(Short.class) || type.equals(Short.TYPE))
						values.put(colName, field.getShort(object));
					else if(type.equals(Double.class) || type.equals(Double.TYPE))
						values.put(colName, field.getDouble(object));
					else if(type.equals(Float.class) || type.equals(Float.TYPE)) 
						values.put(colName, field.getFloat(object));
					else if(type.equals(Byte[].class) || type.equals(byte[].class))
						values.put(colName, (byte[]) field.get(object));
					else if(type.equals(Boolean.class) || type.equals(Boolean.TYPE))
						values.put(colName, field.getBoolean(object) ? 1 : 0);
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
			
			clazz=clazz.getSuperclass();
		} while(clazz!=Object.class && clazz!=null && sorm.checkParentClass());
		
		return values;
	}
	
	/**
	 * Creates a DROP-TABLE-Query for the given class.
	 * This class has to be annotated with {@link SORM}
	 */
	public static String createDropTable(final Class<?> clazz) 
	{
		if(!clazz.isAnnotationPresent(SORM.class))
			throw new SORMException(Type.MISSING_SORM);
		
		STable table=createTableModel(clazz);
		
		return "DROP TABLE IF EXISTS " + table.getName() + ";";
	}
	
	/**
	 * Executes the result of {@link SORMFactory#createDropTable(Class)} on the given Databaseobject.
	 * This method is short for: database.execSQL(createDropTable(clazz));
	 */
	public static void dropTable(final SQLiteDatabase database, final Class<?> clazz)
	{
		database.execSQL(createDropTable(clazz));
	}
	
	/**
	 * Stores the object into the database. The class of the given object
	 * has to be anntotated with {@link SORM}. 
	 * 
	 * @param database a writeable SQLiteDatabase-object
	 * @param object the object should be stored in the database
	 */
	public static void insert(final SQLiteDatabase database, final Object object)
	{
		ContentValues values=toContentValue(object);
		
		String tableName=getTableName(object.getClass());
		
		database.insert(tableName, null, values);
	}
	
	/**
	 * Updates the database with the given object. The given object
	 * has to be annotated with {@link SORM}. The last to params (where & args) 
	 * are used by {@link SQLiteDatabase#update(String, ContentValues, String, String[])}.
	 * 
	 * @param database a writable sqlite-database-object
	 * @param object the object should be stored
	 * @param where see {@link SQLiteDatabase#update(String, ContentValues, String, String[])}
	 * @param args see {@link SQLiteDatabase#update(String, ContentValues, String, String[])}
	 */
	public static void update(final SQLiteDatabase database, final Object object, final String where, final String[] args)
	{
		ContentValues values=toContentValue(object);
		
		String tableName=getTableName(object.getClass());
		
		database.update(tableName, values, where, args);		
	}
	
	/**
	 * Deletes the given Entry in the Database. The given object
	 * has to be annotated with {@link SORM}. The last to params (where & args) 
	 * are used by {@link SQLiteDatabase#delete(String, String, String[])}.
	 * 
	 * @param database a writable sqlite-database-object
	 * @param object the object should be stored
	 * @param where see {@link SQLiteDatabase#delete(String, String, String[])}
	 * @param args see {@link SQLiteDatabase#delete(String, String, String[])}
	 */
	public static void delete(final SQLiteDatabase database, final Object object, final String where, final String[] args)
	{
		String tableName=getTableName(object.getClass());
		
		database.delete(tableName, where, args);
	}
	
	/**
	 * Fetches the tablename from the given class.
	 * 
	 * @param tableClass 
	 * @return the name of the table
	 */
	public static String getTableName(final Class<?> tableClass)
	{
		if(!tableClass.isAnnotationPresent(SORM.class))
			return tableClass.getSimpleName();

		SORM sorm=tableClass.getAnnotation(SORM.class);				
		String name=sorm.value().trim();
		
		if(name.length()==0) {
			Class<?> ref=sorm.ref();
			
			// use referenced Table
			if(ref!=Object.class)
				return getTableName(ref);
			
			return tableClass.getSimpleName();
		}
		return name;
	}
	
	/**
	 * Fetches the columnname from the given field. 
	 * 
	 * @param field
	 * @return the name of the column
	 */
	private static String getColName(final Field field)
	{
		if(!field.isAnnotationPresent(Column.class))
			return field.getName();
		
		String colName=field.getAnnotation(Column.class).value().trim();
		
		if(colName.length()==0)
			return field.getName();
		return colName;
	}	
	
	private static String getColumnsAsSelect(final STable table, final String alias)
	{
		return getColumnsAsSelect(table, alias, null);
	}
	
	
	private static String getColumnsAsSelect(final STable table, final String alias, final String clue)
	{
		String tableAlias=(alias==null) ? table.getName() : alias;
		String colClue=(clue==null) ? "_" : clue;
		
		String select="";
		List<SColumn> columns=table.getColumns();
		
		// Iteratate through Columns
		for(int i=0;i<columns.size();i++) {
			if(i>0) select+=", ";
			
			String colName=columns.get(i).getName();
			select+= tableAlias + ".[" + colName + "] AS " + tableAlias + colClue + colName;
		}
		
		return select;
	}
}
