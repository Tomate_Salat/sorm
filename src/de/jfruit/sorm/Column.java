package de.jfruit.sorm;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This Annotation defines a field as a Column 
 * and allows to override several informations like
 * the name or the type
 * 
 * @author Tomate_Salat
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Column 
{
	/**
	 * Sets the name of the column or leave empty for
	 * using the fieldname
	 */
	String value() default "";
	
	/**
	 * Sets the type of the column or try to fetch it from
	 * the field.
	 */
	String type() default "";
}
