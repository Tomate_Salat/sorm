package de.jfruit.sorm;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Defines the Field as a part of an Index.
 * 
 * @author Tomate_Salat
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Index 
{
	/**
	 * Name of the Index 
	 */	
	String value();	
	
	/**
	 * OrderValue
	 */
	int order() default 1;
}
