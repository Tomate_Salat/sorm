package de.jfruit.sorm;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Required to use this class with SORM. 
 *  
 * @author Tomate_Salat
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface SORM {
	/**
	 * Sets the name of the table or leave it blank for using the 
	 * classname as the name of the table.
	 */
	String value() default "";
	
	/**
	 * If you want to refer to an existing tablestructure, you should use this
	 * parameter to set the tablename.
	 */
	Class<?> ref() default Object.class;
	
	/**
	 * If true => the Annotation {@link Column} is required.
	 * 
	 * Default is <code>false</code>
	 */
	boolean columnAnnoRequired() default false;
	
	/**
	 * If <code>true</code> {@link SORMFactory} will also lookup and handle Superclasses.
	 * 
	 * Default is <code>true</code>
	 */
	boolean checkParentClass() default true;
}
