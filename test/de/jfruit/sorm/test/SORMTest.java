package de.jfruit.sorm.test;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import de.jfruit.sorm.SORMFactory;
import de.jfruit.sorm.model.SColumn;
import de.jfruit.sorm.model.SIndex;
import de.jfruit.sorm.model.STable;
import de.jfruit.sorm.test.classes.ColumnAnnoTest;
import de.jfruit.sorm.test.classes.ColumnAnnoTestV2;
import de.jfruit.sorm.test.classes.IndexTest;
import de.jfruit.sorm.test.classes.JoinFixedVal;
import de.jfruit.sorm.test.classes.MainTable;
import de.jfruit.sorm.test.classes.OrderByTest;
import de.jfruit.sorm.test.classes.UserTable;
import de.jfruit.sorm.test.classes.UsingBase;

public class SORMTest 
{
	@Test
	public void testTableModel()
	{
		// Laden des TableModels
		STable table=SORMFactory.createTableModel(UserTable.class);
		
		// Pr�fung auf Tabellenebene
		assertEquals("user", table.getName());
		assertEquals(2, table.getColumns().size());
		
		// _id-Feld pr�fen
		SColumn column=table.getColumns().get(0); 		// Column: _id [should be]
		assertEquals(true, column.isAutoInc());
		assertEquals("INTEGER", column.getType());
		
		// PK pr�fen
		List<String> primaryKeys=table.getPrimaryKey();
		assertEquals(1, primaryKeys.size());
		assertEquals("_id", primaryKeys.get(0));
		
		// Cache pr�fen
		assertEquals(true, table==SORMFactory.createTableModel(UserTable.class));
		
		// Test create
		assertEquals("CREATE TABLE IF NOT EXISTS user (_id INTEGER auto increment,nutzername varchar(30),PRIMARY KEY(_id));",
				SORMFactory.createTable(UserTable.class));
	}
	
	@Test
	public void testSelectAll()
	{
		assertEquals("SELECT user.* FROM user;", SORMFactory.createSelectAll(UserTable.class));
	}
	
	@Test
	public void testSIndexComperator()
	{
		SIndex[] i={
			new SIndex("a","a", 2),
			new SIndex("a","b", 1),
			new SIndex("a","c", 2)
		};
		
		Arrays.sort(i);
		
		assertEquals(1, i[0].getOrder());
		assertEquals(2, i[1].getOrder());
	}
	
	@Test
	public void testIndexes()
	{
		assertEquals("CREATE TABLE IF NOT EXISTS index (id INTEGER,name TEXT);CREATE INDEX IF NOT EXISTS id_name ON index(name,id);CREATE INDEX IF NOT EXISTS id ON index(id);", SORMFactory.createTable(IndexTest.class));
		assertEquals("CREATE INDEX IF NOT EXISTS id_name ON index(name,id);CREATE INDEX IF NOT EXISTS id ON index(id);", SORMFactory.createIndexStatement(IndexTest.class));
	}
	
	@Test
	public void testColumnAnnotationRequired()
	{
		assertEquals("CREATE TABLE IF NOT EXISTS ColumnAnnoTest (test TEXT);", SORMFactory.createTable(ColumnAnnoTest.class));
		assertEquals("CREATE TABLE IF NOT EXISTS ColumnAnnoTestV2 ();", SORMFactory.createTable(ColumnAnnoTestV2.class));
	}
	
	@Test
	public void testTableExists()
	{
		assertEquals("SELECT COUNT(*) FROM sqlite_master WHERE type='table' AND name='user';", SORMFactory.createCheckTableExists(UserTable.class));
	}
	
	@Test
	public void testDropTable()
	{
		assertEquals("DROP TABLE IF EXISTS user;", SORMFactory.createDropTable(UserTable.class));
	}
	
	@Test
	public void testInherit()
	{
		assertEquals("CREATE TABLE IF NOT EXISTS extend_table (age INTEGER,name TEXT);", SORMFactory.createTable(UsingBase.class));
	}
	
	@Test
	public void testJoinSelect()
	{
		assertEquals("SELECT MainTable.*,table.linkerMaster AS table_linkerMaster, table.id AS table_id, table.name AS table_name,tableNext.linkerMaster AS tableNext_linkerMaster, tableNext.id AS tableNext_id, tableNext.name AS tableNext_name FROM MainTable LEFT JOIN details table ON table.linkerMaster=MainTable.id LEFT JOIN details tableNext ON tableNext.lid=MainTable.id AND tableNext.name=MainTable.name;", SORMFactory.createSelectAll(MainTable.class));
	}
	
	@Test
	public void testJoinFixedVal()
	{
		assertEquals("SELECT JoinFixedVal.*,table.id AS table_id, table.name AS table_name FROM JoinFixedVal LEFT JOIN MainTable table ON table.id='6';",SORMFactory.createSelectAll(JoinFixedVal.class));
	}
	
	@Test
	public void testOrderBy()
	{
		assertEquals("SELECT OrderByTest.* FROM OrderByTest ORDER BY a ASC, b ASC;", SORMFactory.createSelectAll(OrderByTest.class));
	}
}
