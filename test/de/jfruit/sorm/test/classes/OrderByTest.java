package de.jfruit.sorm.test.classes;

import de.jfruit.sorm.Order;
import de.jfruit.sorm.OrderBy;
import de.jfruit.sorm.SORM;

@SORM
public class OrderByTest
{
	@OrderBy
	private int a;
	
	@OrderBy(Order.ASC)
	private String b;
}
