package de.jfruit.sorm.test.classes;

public class Base
{
	private String name;
	
	public String getName()
	{
		return name;
	}
	
	public void setName(final String name)
	{
		this.name = name;
	}
}