package de.jfruit.sorm.test.classes;

import de.jfruit.sorm.Column;
import de.jfruit.sorm.Index;
import de.jfruit.sorm.Indexes;
import de.jfruit.sorm.SORM;

@SORM("index")
public class IndexTest
{
	@Column
	@Indexes({
		@Index("id"),
		@Index(value="id_name",order=2)
	})
	private long id;
	
	@Column @Index("id_name")
	private String name;
}