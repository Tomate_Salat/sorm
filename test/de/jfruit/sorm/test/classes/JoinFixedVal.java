package de.jfruit.sorm.test.classes;

import de.jfruit.sorm.JoinColumn;
import de.jfruit.sorm.OneToOne;
import de.jfruit.sorm.SORM;

@SORM
public class JoinFixedVal
{
	private int id;
	
	@OneToOne @JoinColumn(name="id", toValue="6")
	private MainTable table;

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}
}
