package de.jfruit.sorm.test.classes;

import de.jfruit.sorm.SORM;

@SORM("details")
public class JoinTable
{
	private int linkerMaster;
	
	int id;
	String name;

	public int getLinkerMaster()
	{
		return linkerMaster;
	}

	public void setLinkerMaster(final int linkerMaster)
	{
		this.linkerMaster = linkerMaster;
	}

	public String getName()
	{
		return name;
	}

	public void setName(final String name)
	{
		this.name = name;
	}
	
	
}
