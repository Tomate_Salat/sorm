package de.jfruit.sorm.test.classes;

import de.jfruit.sorm.SORM;

@SORM("extend_table")
public class UsingBase extends Base
{
	private int age;
	
	public void setAge(final int age)
	{
		this.age = age;
	}
	
	public int getAge()
	{
		return age;
	}
}