package de.jfruit.sorm.test.classes;

import de.jfruit.sorm.JoinColumn;
import de.jfruit.sorm.JoinColumns;
import de.jfruit.sorm.OneToOne;
import de.jfruit.sorm.SORM;

@SORM
public class MainTable
{
	int id;
	String name;
	
	@OneToOne
	@JoinColumn(name="id", referencedColumnName="linkerMaster")
	JoinTable table;
	
	@OneToOne
	@JoinColumns({
		@JoinColumn(name="id", referencedColumnName="lid"),
		@JoinColumn(name="name")
	})
	JoinTable tableNext;
}
