package de.jfruit.sorm.test.classes;

import de.jfruit.sorm.SORM;

@SORM(columnAnnoRequired=true)
public class ColumnAnnoTestV2
{
	private String test;
	
	public String getTest() {
		return test;
	}
}