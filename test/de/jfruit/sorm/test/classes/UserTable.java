package de.jfruit.sorm.test.classes;

import de.jfruit.sorm.Autoinc;
import de.jfruit.sorm.Column;
import de.jfruit.sorm.PK;
import de.jfruit.sorm.SORM;

@SORM("user")
public class UserTable
{
	@PK @Autoinc @Column("_id")
	private long id;
	
	@Column(value="nutzername", type="varchar(30)")
	private String name;
}